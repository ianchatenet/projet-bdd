CREATE TABLE Customers(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    CustomerName VARCHAR(50),
    CustomerFirstName VARCHAR(50),
    CustomerAddress VARCHAR(150),
    CustomerCity VARCHAR(100),
    CustomerCountry VARCHAR(50),
    CustomerPhone VARCHAR(20),
    CustomerMail VARCHAR(200)
);

CREATE TABLE Products(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    ProductName VARCHAR(100),
    ProductBrand INTEGER,
    ProductDescri VARCHAR(300),
    ProductLink VARCHAR(300),
    ProductTags INTEGER,
    ProductPrice FLOAT,
    FOREIGN KEY(ProductBrand) REFERENCES Brands(Id),
    FOREIGN KEY(ProductTags) REFERENCES Tags(Id)
);

CREATE TABLE Brands(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    BrandName VARCHAR(30)
);

CREATE TABLE Tags(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    TagName VARCHAR(100)
);

CREATE TABLE Baskets(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    Customer INTEGER,
    Product INTEGER,
    Color INTEGER,
    Quantity INTEGER,
    FOREIGN KEY(Color) REFERENCES Colors(Id),
    FOREIGN KEY(Product) REFERENCES Products(Id),
    FOREIGN KEY(Customer) REFERENCES Customers(Id)
);


CREATE TABLE Colors(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    ColorName VARCHAR(20)
);

CREATE TABLE Sells(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    Basket INTEGER,
    SellDate DATETIME,
    Amount INTEGER,
    FOREIGN KEY(Basket) REFERENCES Baskets(Id)
);