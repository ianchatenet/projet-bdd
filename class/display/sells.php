<?php
namespace App\display ;

class sells{

    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    }

    public function displaySells(){
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $sql1='SELECT COUNT(*) FROM Sells';
        $exe1 = $this->pdo->prepare($sql1);
        $exe1->execute();
        $nbrElmts = $exe1->fetch();
       
        
        $limit = 20;
        $debut = ($page - 1) * $limit;
        $nbrPages = ceil($nbrElmts[0]/$limit);
        

        $tmpValue = $nbrPages-$page;
        $displayedPage =6+$page;
        

        $sql = 'SELECT * FROM Sells LEFT JOIN Baskets ON Sells.Basket=Baskets.Id  LEFT JOIN Colors ON Baskets.Color=Colors.Id LEFT JOIN Products ON Baskets.Product = Products.Id LIMIT :limite OFFSET :debut';
        $exe = $this->pdo->prepare($sql);
        $exe->execute([
            ':limite'=>$limit,
            ':debut'=>$debut
        ]);
        ?>
        
        <div class="container">
            <div class="row border text-center bg-info">
                    <div class="col-sm-1 border-right">
                        Panier
                    </div>

                    <div class="col-sm-3 border-right">
                        Date de vente
                    </div>

                    <div class="col-sm-1 border-right">
                        Clients
                    </div>

                    <div class="col-sm-4 border-right">
                        Produits
                    </div>

                    <div class="col-sm-1 border-right">
                        Quantité
                    </div>

                    <div class="col-sm-1 border-right">
                        Couleur
                    </div>

                    <div class="col-sm-1">
                        Montant
                    </div>

                    
            </div>
        <?php
        while($element = $exe->fetch()){?>

            
           
            <div class="row border text-center">
                    <div class="col-sm-1 border-right">
                        <?php  echo $element['Basket'];?>
                    </div>

                    <div class="col-sm-3 border-right">
                        <?php  echo $element['SellDate'];?>
                    </div>

                    <div class="col-sm-1 border-right">
                        <?php  echo $element['Customer'];?>
                    </div>

                    <div class="col-sm-4 border-right">
                        <?php  echo $element['ProductName'];?>
                    </div>

                    <div class="col-sm-1 border-right">
                        <?php  echo $element['Quantity'];?>
                    </div>

                    <div class="col-sm-1 border-right">
                        <?php  echo $element['ColorName'];?>
                    </div>

                    <div class="col-sm-1">
                        <?php  echo $element['Amount'];?>
                    </div>
                    

                </div> 
            
                 
       <?php
        }

        
        ?>
    
        <div class="row text-center d-flex justify-content-center mt-2">
        
        <nav>
                <ul class="pagination">
                <li class="page-item"><a href="?page=1" class="page-link">Page 1</a></li>
                <?php
                    if($page!=1){
                    ?><li class="page-item"><a href="?page=<?php echo $page-1; ?>" class="page-link">Page précédente <?php echo $i ?></a></li>
                    <?php
                }

        ?> 
                
                <?php
                    for($i=$page+1;$i<=$displayedPage;$i++){

                           if($i-1==$nbrPages){
                           break;
                            }
                        ?>
                    <li class="page-item"><a href="?page=<?php echo $i; ?>" class="page-link">Page <?php echo $i ?></a></li>

             
           
           <?php
        } 
            if($page!=$nbrPages){
            ?><li class="page-item"><a href="?page=<?php echo $page+1; ?>" class="page-link">Page suivante</a></li>
            <?php
         }

        ?> 
    

                    
                    <li class="page-item"><a href="?page=<?php echo $nbrPages; ?>" class="page-link">Dernière page</a></li>
                </ul>
            </nav>
        </div>
    </div> <!-- container-->

    <?php
    }
}
?>