<?php
namespace App\display ;

class product{

    private $pdo;
    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function displayProduct(){
        
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $sql1='SELECT COUNT(*) FROM Products';
        $exe1 = $this->pdo->prepare($sql1);
        $exe1->execute();
        $nbrElmts = $exe1->fetch();
        
        
        $limit = 4;
        $debut = ($page - 1) * $limit;
        $nbrPages = ceil($nbrElmts[0]/$limit);
        

        $tmpValue = $nbrPages-$page;
        $displayedPage =6+$page;
        

        $sql = 'SELECT * FROM Products LEFT JOIN Brands ON Products.ProductBrand = Brands.Id LEFT JOIN Tags ON Products.ProductTags = Tags.ID LIMIT :limite OFFSET :debut';
        $exe = $this->pdo->prepare($sql);
        $exe->execute([
            ':limite'=>$limit,
            ':debut'=>$debut
        ]);
?>
         
    <div class="container">
                
            <div class="row border text-center bg-info">
                    <div class="col-sm-1 border-right">
                        Produits
                    </div>

                    <div class="col-sm-1 border-right">
                        Marque
                    </div>

                    <div class="col-sm-4 border-right">
                        Description
                    </div>

                    <div class="col-sm-2 border-right">
                        Lien
                    </div>

                    <div class="col-sm-2 border-right">
                        Tags
                    </div>

                    <div class="col-sm-2 border-right">
                        Prix
                    </div>
                    
            </div>
        <?php
        while($element = $exe->fetch()){
            ?>

            
           
            <div class="row border text-center">
                    <div class="col-sm-1 border-right">
                        <?php  echo $element['ProductName'];?>
                    </div>

                    <div class="col-sm-1 border-right">
                        <?php  echo $element['BrandName'];?>
                    </div>

                    <div class="col-sm-4 border-right">
                        <?php  echo $element['ProductDescri'];?>
                    </div>

                    <div class="col-sm-2 border-right">
                        <?php  echo $element['ProductLink'];?>
                    </div>

                    <div class="col-sm-2 border-right">
                        <?php  echo $element['TagName'];?>
                    </div>

                    <div class="col-sm-2 border-right">
                        <?php  echo $element['ProductPrice'];?>
                    </div>                    

                </div> 
            
                 
       <?php
        }

        
        ?>
    
        <div class="row text-center d-flex justify-content-center mt-2">
        
             <nav>
                <ul class="pagination">
                <li class="page-item"><a href="?page=1" class="page-link">Page 1</a></li>
                <?php
                    if($page!=1){
                    ?><li class="page-item"><a href="?page=<?php echo $page-1; ?>" class="page-link">Page précédente <?php echo $i ?></a></li>
                    <?php
                }

        ?> 
                
                <?php
                    for($i=$page+1;$i<=$displayedPage;$i++){

                           if($i-1==$nbrPages){
                           break;
                            }
                        ?>
                    <li class="page-item"><a href="?page=<?php echo $i; ?>" class="page-link">Page <?php echo $i ?></a></li>

             
           
           <?php
        } 
            if($page!=$nbrPages){
            ?><li class="page-item"><a href="?page=<?php echo $page+1; ?>" class="page-link">Page suivante</a></li>
            <?php
         }

        ?> 
    

                    
                    <li class="page-item"><a href="?page=<?php echo $nbrPages; ?>" class="page-link">Dernière page</a></li>
                </ul>
            </nav>
        </div>
    </div> <!-- container-->

    <?php
    }
}
?>