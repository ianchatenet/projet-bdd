<?php 
namespace App\parse;

class products{

    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    } 

    public function getProducts(){
       
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("database16.xlsx");
        $sheet = $spreadsheet->getSheet(2);
        $highestRow = $sheet->getHighestRow();

        $Name= array();
        $Desc = array();
        $Url = array();
        $Price = array();
        $Brand = array();
        $Tags = array();


        for($i=1; $i<$highestRow+1; $i++){
            
            $cellName = $sheet->getCell('B'.$i);
            $valueName = $cellName->getValue();

            $cellDesc = $sheet->getCell('D'.$i);
            $valueDesc = $cellDesc->getValue();

            $cellUrl = $sheet->getCell('E'.$i);
            $valueUrl = $cellUrl->getValue();

            $cellPrice = $sheet->getCell('G'.$i);
            $valuePrice = $cellPrice->getValue();

            $cellBrand = $sheet->getCell('C'.$i);
            $valueBrand = $cellBrand->getValue();

            $cellTags = $sheet->getCell('F'.$i);
            $valueTags = $cellTags->getValue();
            
            array_push($Name,$valueName);
            array_push($Desc,$valueDesc);
            array_push($Url,$valueUrl);
            array_push($Price,$valuePrice);
            array_push($Brand,$valueBrand);
            array_push($Tags,$valueTags);
        }

        $uniqueBrand=array_unique($Brand);

        $uniqueName =array_unique($Name);
        $uniqueDesc = array_unique($Desc);
        $uniqueUrl = array_unique($Url);
        $uniquePrice = array_unique($Price);
        $uniqueTags = array_unique($Tags);
        $keyBrand = array_keys($uniqueBrand);
        $keyTags = array_keys($uniqueTags);
        for($i=0; $i<$highestRow;$i++){
            
            $sql = 'INSERT INTO Products(ProductName,ProductBrand,ProductDescri,ProductLink,ProductTags,ProductPrice)
            VALUES(:ProductName,:ProductBrand,:ProductDescri,:ProductLink,:ProductTags,:ProductPrice)';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':ProductName'=>$uniqueName[$i],
                ':ProductBrand'=>$keyBrand[$i]+1,
                ':ProductDescri'=>$uniqueDesc[$i],
                ':ProductLink'=>$uniqueUrl[$i],
                ':ProductTags'=>$keyTags[$i]+1,
                ':ProductPrice'=>$uniquePrice[$i]
            ]);
        }
    }
}