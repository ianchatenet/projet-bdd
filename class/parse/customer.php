<?php 
namespace App\parse;

class customer{

    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    } 

    public function getCustomer(){
       
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("database16.xlsx");
        $sheet = $spreadsheet->getSheet(0);

        $highestRow = $sheet->getHighestRow();
        $Name= array();
        $FirstName = array();
        $Address = array();
        $City = array();
        $Country = array();
        $Phone = array();
        $Mail = array();
        
        for($i=1; $i<$highestRow+1; $i++){
            
            $cellName = $sheet->getCell('C'.$i);
            $valueName = $cellName->getValue();

            $cellFirstName = $sheet->getCell('B'.$i);
            $valueFirstName = $cellFirstName->getValue();

            $cellAddress = $sheet->getCell('G'.$i);
            $valueAddress = $cellAddress->getValue();

            $cellCity = $sheet->getCell('F'.$i);
            $valueCity = $cellCity->getValue();

            $cellCountry = $sheet->getCell('H'.$i);
            $valueCountry = $cellCountry->getValue();

            $cellPhone = $sheet->getCell('E'.$i);
            $valuePhone = $cellPhone->getValue();

            $cellMail = $sheet->getCell('D'.$i);
            $valueMail = $cellMail->getValue();
           
            array_push($Name,$valueName);
            array_push($FirstName,$valueFirstName);
            array_push($Address,$valueAddress);
            array_push($City,$valueCity);
            array_push($Country,$valueCountry);
            array_push($Phone,$valuePhone);
            array_push($Mail,$valueMail);
        }

        $uniqueAddress = array_unique($Address);
        $uniquePhone = array_unique($Phone);
        $uniqueMail = array_unique($Mail);

        for($i=0; $i<$highestRow;$i++){
            
           $sql = 'INSERT INTO Customers(CustomerName,CustomerFirstName,CustomerAddress,CustomerCity,CustomerCountry,CustomerPhone,CustomerMail)
               VALUES(:CustomerName,:CustomerFirstName,:CustomerAddress,:CustomerCity,:CustomerCountry,:CustomerPhone,:CustomerMail)';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':CustomerName'=>$Name[$i],
                ':CustomerFirstName'=>$FirstName[$i],
                ':CustomerAddress'=>$uniqueAddress[$i],
                ':CustomerCity'=>$City[$i],
                ':CustomerCountry'=>$Country[$i],
                ':CustomerPhone'=>$uniquePhone[$i],
                ':CustomerMail'=>$uniqueMail[$i],
            ]);

            echo $Name[$i];  
            echo $FirstName[$i]; 
            echo $uniqueAddress[$i];
            echo $City[$i];
            echo $Country[$i];
            echo $uniquePhone[$i];
            echo $uniqueMail[$i];
        }
    } 
}