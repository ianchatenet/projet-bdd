<?php 

namespace App\parse;
require 'vendor/autoload.php';



class baskets{

    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    }


    public function getBaskets(){
       
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("database16.xlsx");
        
        
        
        $sheet = $spreadsheet->getSheet(1);

        $highestRow = $sheet->getHighestRow();
        $Colors= array();
        $Custo = array();
        $Product = array();
        for($i=1; $i<$highestRow+1; $i++){

            $cellCusto = $sheet->getCell('B'.$i);
            $valueCusto = $cellCusto->getValue();
            array_push($Custo,str_replace("customer-",'',$valueCusto));

            $cellProduct = $sheet->getCell('C'.$i);
            $valueProduct = $cellProduct->getValue();
            array_push($Product,str_replace("product-",'',$valueProduct));

            $cellColor = $sheet->getCell('E'.$i);
            $valueColor = $cellColor->getValue();
            array_push($Colors,$valueColor);
        }
            
        $idColor = array();
       for($i=0;$i<$highestRow;$i++){

        $sql1 ='SELECT Id FROM Colors WHERE ColorName LIKE :colors';
            $stmt = $this->pdo->prepare($sql1);
            $stmt->execute([
                ':colors'=>$Colors[$i]
            ]);
            $colorId = $stmt->fetch();
             array_push($idColor,$colorId['Id']);
             
             $random = random_int(1,5);

            $sql = "INSERT INTO Baskets(Customer,Product,Color,Quantity) VALUES (:Customer,:Product,:Color,:Quantity)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
               ':Customer'=>$Custo[$i],
               ':Product'=>$Product[$i],
                ':Color'=>$idColor[$i],
                ':Quantity'=>$random
            ]); 
          
          echo $idColor[$i];      
       }
       
       
      // print_r($idColor[$i]);
       
       
    }

}
