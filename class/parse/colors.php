<?php 

namespace App\parse;
require 'vendor/autoload.php';



class Colors{

    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    }


    public function getColors(){
       
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("database16.xlsx");
        
        $sheet = $spreadsheet->getSheet(1);

        $highestRow = $sheet->getHighestRow();
        $Colors= array();
        
        for($i=1; $i<$highestRow+1; $i++){
            
            $cell = $sheet->getCell('E'.$i);
            $value = $cell->getValue();
            //echo $value;
            array_push($Colors,$value);
            
        }


        $unique =array_unique($Colors);
        //var_dump($unique);

        foreach($unique as $value){
            $sql = 'INSERT INTO Colors(ColorName)'.'VALUES(:ColorName)';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':ColorName'=>$value,
            ]);
            echo $value;
        }
       
    
    } 
}