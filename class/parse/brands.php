<?php 

namespace App\parse;
require 'vendor/autoload.php';



class brands{

    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    }


    public function getBrands(){
       
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("database16.xlsx");
        
        $sheet = $spreadsheet->getSheet(2);

        $highestRow = $sheet->getHighestRow();
        $Brands= array();
        for($i=1; $i<$highestRow+1; $i++){
            
            $cell = $sheet->getCell('c'.$i);
            $value = $cell->getValue();
            //echo $value;
            array_push($Brands,$value);
            $unique =array_unique($Brands);
        }

        //var_dump($unique);

        foreach($unique as $value){
            $sql = 'INSERT INTO Brands(BrandName)'.'VALUES(:BrandName)';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':BrandName'=>$value,
            ]);
            echo $value;
        }
       
    
    } 
}