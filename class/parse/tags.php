<?php 

namespace App\parse;
require 'vendor/autoload.php';



class tags{

    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    }

    public function getTags(){
       
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("database16.xlsx");
        $sheet = $spreadsheet->getSheet(2);

        $highestRow = $sheet->getHighestRow();
        $Tags= array();
        for($i=1; $i<$highestRow+1; $i++){
            
            $cell = $sheet->getCell('F'.$i);
            $value = $cell->getValue();
           
            array_push($Tags,$value);
            $unique =array_unique($Tags);
        }
        foreach($unique as $value){
            $sql = 'INSERT INTO Tags(TagName)'.'VALUES(:TagName)';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':TagName'=>$value,
            ]);
            echo $value;
        }
    } 
}