<?php 
namespace App\parse;

class sells{
    
    private $pdo;
    public function __construct($pdo){
        $this->pdo =$pdo;
    }


    public function getSells(){
        set_time_limit(0);
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("database16.xlsx");
        
        $sheet = $spreadsheet->getSheet(1);

        $highestRow = $sheet->getHighestRow();

        $basket= array();
        $idBasket = array();
        $date = array();
        $quantity = array();
        $Quantity = array();
        $price = array();
        $Price = array();
        $product = array();
        $Product = array();



        for($i=1; $i<$highestRow+1; $i++){
            
            $cellDate = $sheet->getCell('D'.$i);
            $valueDate = $cellDate->getValue();
            //echo $valueDate;
            array_push($date,$valueDate); 
        }

        

        for($i=0;$i<$highestRow;$i++){
            
            $sql1 ='SELECT Id FROM Baskets WHERE Id LIKE :id ';
            $stmt1 = $this->pdo->prepare($sql1);
            $stmt1->execute([
                ':id'=>$i+1
            ]);
            $basket = $stmt1->fetch();
             array_push($idBasket,$basket['Id']);


            $sql2 ='SELECT Quantity FROM Baskets WHERE Id LIKE :id ';
            $stmt2 = $this->pdo->prepare($sql2);
            $stmt2->execute([
                ':id'=>$i+1
            ]);
            $quantity = $stmt2->fetch();
             array_push($Quantity,$quantity['Quantity']);

            $sql3 ='SELECT Product FROM Baskets WHERE Id LIKE :id';
            $stmt3 = $this->pdo->prepare($sql3);
            $stmt3->execute([
                ':id'=>$i+1
            ]);
            $product = $stmt3->fetch();
             array_push($Product,$product['Product']);



             $sql4 ='SELECT ProductPrice FROM Products WHERE Id LIKE :id ';
            $stmt4 = $this->pdo->prepare($sql4);
            $stmt4->execute([
                ':id'=>$Product[$i]
            ]);
            $price = $stmt4->fetch();
             array_push($Price,$price['ProductPrice']);
               
            $sql = "INSERT INTO Sells(Basket,Selldate,Amount) VALUES (:Basket,:Selldate,:Amount)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':Basket'=>$idBasket[$i],
                ':Selldate'=>$date[$i],
                ':Amount'=>$Quantity[$i]*$Price[$i]
            ]);
          
          
        }
        
        
    } 
}