<?php
namespace App\connect;

class connectionPages{
    
    private $pdo;
    
     
    public function connect() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . configPages:: PATH_TO_DATABASE);
        }
        return $this->pdo;
    }

}