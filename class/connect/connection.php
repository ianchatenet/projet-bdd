<?php
namespace App\connect;

class connection{
    
    private $pdo;
    
     
    public function connect() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . config:: PATH_TO_DATABASE);
        }
        return $this->pdo;
    }

}