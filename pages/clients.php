<?php

require('../vendor/autoload.php');
use App\connect\connectionPages;
use App\display\customer;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Base de donnée</title>
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container text-center">
            <div class="row">

                    <div class="col-sm">
                        <a href="../index.html">Acceuil</a>
                    </div>

                    <div class="col-sm">
                        <a href="produits.php">Produits</a>
                    </div>

                    <div class="col-sm">
                        <a href="ventes.php">Ventes</a>
                    </div>

            </div>
        </div>
   
    <?php

        $pdo = (new connectionPages())->connect();

        // if ($pdo != null)
        //     echo 'Connected to the SQLite database successfully!';
        // else
        //     echo 'Whoops, could not connect to the SQLite database!';


        $truc = (new customer($pdo))->displayCustomer();

    ?>
    </body>
</html>